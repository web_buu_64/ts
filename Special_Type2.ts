let w : unknown = 1;
w = "String";
w = {
    runANonExistendMethod: ()  => {
        console.log("I think therefore I am");
    }
} as {runANonExistendMethod: ()  => void }

if (typeof w === "object" && w !== null) {
    (w as {runANonExistendMethod: ()  => void}).runANonExistendMethod();
}